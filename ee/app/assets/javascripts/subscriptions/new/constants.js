export const TAX_RATE = 0;

export const PurchaseEvent = Object.freeze({
  ERROR: 'error',
  ERROR_RESET: 'error-reset',
});
